import rootReducer from "./reducers"
import storage from "redux-persist/lib/storage"
import logger, { createLogger } from "redux-logger"
import thunk from "redux-thunk"
import { createStore, applyMiddleware } from "redux"
import { persistStore, persistReducer } from 'redux-persist'
import { UPDATE_TIME } from "./actions/player_actions"

const persistConfig = {
    key: "root",
    storage
}
const persistentReducer = persistReducer(persistConfig, rootReducer)
const nolog_logger = createLogger({
    predicate: (getState, action) => action.type !== UPDATE_TIME
})

let store = createStore(persistentReducer, applyMiddleware(nolog_logger, thunk))
let persistor = persistStore(store)

export default { store, persistor }