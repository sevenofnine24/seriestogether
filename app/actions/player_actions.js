export const LOAD_MEDIA = "LOAD_MEDIA"
export const UPDATE_TIME = "UPDATE_TIME"
export const RESET_STATS = "RESET_STATS"
export const UPDATE_PLAYING = "UPDATE_PLAYING"
import axios from "../axios"

export function loadMedia(meta, media) {
    return {
        type: LOAD_MEDIA,
        meta, media
    }
}

export function ajaxEpisode(media) {
    return dispatch => {
        axios.get(["/media", media.series, media.season, media.episode].join("/")).then(r => {
            dispatch(resetStats())
            dispatch(loadMedia(r.data.meta, r.data.media))
        })
    }
}

export function ajaxMovie(sid) {
    return dispatch => {
        axios.get("/media/" + sid).then(r => {
            dispatch(resetStats())
            dispatch(loadMedia(r.data.meta, r.data.media))
        })
    }
}

export function resetStats() {
    return {
        type: RESET_STATS
    }
}

export function updateTime(time) {
    return {
        type: UPDATE_TIME,
        time
    }
}

export function updatePlaying(playing) {
    return {
        type: UPDATE_PLAYING,
        playing
    }
}