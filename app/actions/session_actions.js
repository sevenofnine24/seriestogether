import axios from "../axios"

export const JOIN_SESSION = "JOIN_SESSION"
export const LEAVE_SESSION = "LEAVE_SESSION"
//export const CREATE_SESSION = "CREATE_SESSION"

export function socketJoinSession(session_id) {
    return (dispatch, getState) => {
        const tosend = {
            master_id: getState().session_reducer.own_id,
            playing: getState().player_reducer.media
        }
        window.socket.emit("join", session_id)
        dispatch(joinSession(session_id, true))
    }
}

export function joinSession(session_id, master = false) {
    return {
        type: JOIN_SESSION,
        session_id, master 
    }
}

export function socketLeaveSession(session_id) {
    return (dispatch, getState) => {
        window.socket.emit("leave", session_id)
        dispatch(leaveSession(session_id, true))
    }
}

export function leaveSession() {
    return {
        type: LEAVE_SESSION
    }
}