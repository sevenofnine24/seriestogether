import React, { Component } from "react"
import { Link, NavLink } from "react-router-dom"
import ClearState from "../components/ClearState"

class Header extends Component {
    render() {
        return <div id="header">
            <div className="wrapper">
                <div className="header_title">SeriesTogether</div>
                <nav>
                    <NavLink to="/" exact>Home</NavLink>
                    <NavLink to="/player">Player</NavLink>
                    <NavLink to="/session">Session</NavLink>
                    <ClearState />
                </nav>
            </div>
        </div>
    }
}

export default Header