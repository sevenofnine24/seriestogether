import React, { Component } from "react"
import ReactDOM from "react-dom"
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom"
import "./style.scss"
import Header from "./layout/Header"
import Home from "./pages/Home"
import Footer from "./layout/Footer"
import Series from "./pages/Series"
import Player from "./pages/Player"
import Session from "./components/Session"
import { Provider } from "react-redux"
import { PersistGate } from 'redux-persist/integration/react'
import store_obj from "./store"
import Blocking from "./components/Blocking"
import io from "socket.io-client"
import { updatePlaying, updateTime } from "./actions/player_actions"

window.socket = io("ws://188.157.162.32:3001")
window.socket.on("connect", () => console.log("socket connected"))

window.socket.on("play", () => {
    store_obj.store.dispatch(updatePlaying(true))
})

window.socket.on("pause", () => {
    store_obj.store.dispatch(updatePlaying(false))
})

window.socket.on("seek", time => {
    store_obj.store.dispatch(updateTime(time))
})

class App extends Component {
    render() {
        //if (!this.props.password) return <Blocking />
        return <Provider store={store_obj.store}>
            <PersistGate loading={null} persistor={store_obj.persistor}>
                <Router>
                    <Header />
                    <Switch>
                        <Route path="/" exact children={<Home />} />
                        <Route path="/series/:id" children={<Series />} />
                        <Route path="/player" exact children={<Player />} />
                        <Route path="/player/:movie" exact children={<Player />} />
                        <Route path="/player/:series/:season/:episode" children={<Player />} />
                        <Route path="/session" children={<Session />} />
                    </Switch>
                    <Footer />
                </Router>
            </PersistGate>
        </Provider>
    }
}

ReactDOM.render(<App />, document.getElementById("app"))