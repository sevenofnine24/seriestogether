import { combineReducers } from "redux"
import player_reducer from "./player_reducer"
import session_reducer from "./session_reducer"

export default combineReducers({
    player_reducer,
    session_reducer
})