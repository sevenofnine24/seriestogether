import { JOIN_SESSION, LEAVE_SESSION } from "../actions/session_actions"

const chance = require("chance")()

const initialState = {
    own_id: chance.string({ length: 8 }),
    session_id: null,
    playing: {}
}

export default (state = initialState, action) => {
    switch(action.type) {
        case JOIN_SESSION:
            return Object.assign({}, state, {
                session_id: action.session_id
            })
        case LEAVE_SESSION:
            return Object.assign({}, state, {
                session_id: null
            })
        default:
            return state
    }
}