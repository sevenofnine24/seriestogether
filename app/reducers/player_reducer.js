import { LOAD_MEDIA, UPDATE_TIME, RESET_STATS, UPDATE_PLAYING } from "../actions/player_actions"

const initialState = {
    media: {},
    meta: {},
    playing: false,
    time: 0
}

export default (state = initialState, action) => {
    switch(action.type) {
        case LOAD_MEDIA:
            return Object.assign({}, state, { 
                media: action.media,
                meta: action.meta
            })
        case UPDATE_TIME:
            return Object.assign({}, state, {
                time: action.time
            })
        case UPDATE_PLAYING:
            return Object.assign({}, state, {
                playing: action.playing
            })
        case RESET_STATS:
            return Object.assign({}, state, {
                playing: false,
                time: 0
            })
        default:
            return state
    }
}