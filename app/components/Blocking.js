import React, { Component } from "react"

class Blocking extends Component {
    render() {
        return <div id="blocking">
            <div className="blocking_title">This site is protected.</div>
            <div className="blocking_subtitle">Enter a valid password below.</div>
            <input type="password" className="blocking_input" placeholder="Password" />
            <button className="blocking_button">Enter</button>
        </div>
    }
}

export default Blocking