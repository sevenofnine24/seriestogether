import React, { Component, Fragment } from "react"
import { connect } from "react-redux"
import { socketJoinSession, socketLeaveSession } from "../actions/session_actions"

class Session extends Component {
    constructor(props) {
        super(props)
        this.state = {
            input_txt: this.props.session_id ?? ""
        }
    }
    render() {
        return <div id="session" className="wrapper">
            <div className="session_title title">Session</div>
            <div className="session_current"><b>Current session:</b> {this.props.session_id ?? "No session"}</div>
            <div className="session_join_div">
                <input type="text" className="session_join_input" value={this.state.input_txt} onChange={e => this.inputChange(e)} disabled={this.props.session_id != null} />
                {this.buttons()}
            </div>
            {this.sessionStats()}
        </div>
    }
    buttons() {
        if (this.props.session_id) {
            return <button className="session_leave_btn" onClick={() => this.props.store_leave_session()}>Leave</button>
        } else {
            return <button className="session_join_btn" onClick={() => this.props.store_join_session(this.state.input_txt)}>Join</button>
        }
    }
    inputChange(e) {
        this.setState({
            input_txt: e.target.value
        })
    }

    sessionStats() {
        if (this.props.session_id) {
            return <div className="session_stats">
                <div className="session_stats_title">Session stats</div>
            </div>
        } else {
            return null
        }
    }
}

const mapStateToProps = ({ session_reducer }) => {
    return {
        own_id: session_reducer.own_id,
        session_id: session_reducer.session_id,
        playing: session_reducer.playing
    }
}

const mapDispatchToProps = dispatch => {
    return {
        store_join_session: (session_id) => dispatch(socketJoinSession(session_id)),
        store_leave_session: (session_id) => dispatch(socketLeaveSession(session_id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Session)