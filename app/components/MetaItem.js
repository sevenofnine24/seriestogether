import React, { Component } from 'react'
import { Link } from "react-router-dom"

class MetaItem extends Component {
    render() {
        return <Link
            className="meta"
            style={{backgroundImage: "url('" + this.props.image + "')"}}
            key={this.props.title}
            to={this.getLink()}
        >
            <div className="meta_title">{this.props.title}</div>
        </Link>
    }
    
    getLink() {
        if (this.props.type == "series") return "/series/" + this.props.sid
        else return "/player/" + this.props.sid
    }
}

export default MetaItem