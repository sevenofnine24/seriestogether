import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PURGE } from 'redux-persist'

class ClearState extends Component {
    render() { 
        return <a onClick={() => this.props.clearState()}>Clear state</a>
    }
}

const mapDispatchToProps = dispatch => {
    return {
        clearState: () => {
            dispatch({
                type: PURGE,
                key: "root",
                result: () => null
            })
            location.reload()
        }
    }
}

export default connect(null, mapDispatchToProps)(ClearState)