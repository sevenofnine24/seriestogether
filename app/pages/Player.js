import React, { Component } from "react"
import Hls from "hls.js"
import { withRouter, Link } from "react-router-dom"
import axios from "../axios"
import { connect } from "react-redux"
import { ajaxEpisode, updateTime, updatePlaying } from "../actions/player_actions"
import session_reducer from "../reducers/session_reducer"

class Player extends Component {
    constructor(props) {
        super(props)
        this.state = {
            hls: new Hls(),
            loaded: null
        }

        this.checkParams()
        this.video = React.createRef()
    }
    componentDidMount() {
        this.setUpIO()
        this.loadVideo()
        this.state.hls.attachMedia(this.video.current)
        this.state.hls.on(Hls.Events.MEDIA_ATTACHED, () => {
            if (this.props.playing) this.video.current.play()
            if (this.props.time > 0) this.video.current.currentTime = this.props.time
        })
    }
    componentDidUpdate() {
        if (this.props.media.sid != this.state.loaded) {
            this.state.hls.attachMedia(this.video.current)
            this.loadVideo() // update player
        }
    }
    render() {
        return <div id="player" className="wrapper">
            {this.conditionalPlayer()}
        </div>
    }

    conditionalPlayer() {
        if (this.props.media.sid != null) {
            return <div>
                <div className="player_title title">{this.props.meta.title}</div>
                <div className="player_season">
                    Season {this.props.media.season} Episode {this.props.media.episode}
                    <i>&nbsp;-&nbsp;{this.props.media.title}</i>
                </div>
                <video
                    onTimeUpdate={e => this.timeUpdate(e)}
                    onPlay={e => this.playingUpdate(e)}
                    onPause={e => this.playingUpdate(e)}
                    ref={this.video}
                    id="video"
                controls>
                    <track label="English" kind="subtitles" srcLang="en" src={this.path + "/en.vtt"} />
                    <track label="Magyar" kind="subtitles" srcLang="hu" src={this.path + "/hu.vtt"} default />
                </video>
            </div>
        } else {
            return <div>
                Nothing is playing.
            </div>
        }
    }

    timeUpdate(e) {
        this.props.store_update_time(e.target.currentTime)
    }
    playingUpdate(e) {
        window.socket.emit(e.target.paused ? "pause" : "play", this.props.session_id)
        this.props.store_update_playing(!e.target.paused)
    }

    checkParams() {
        const { movie, series, season, episode } = this.props.match.params

        if (movie) {
            this.props.store_load_movie(movie)
        } else if (series) {
            this.props.store_load_ep(series, season, episode)
        }
    }

    loadVideo() {
        if (this.props.meta.type == "series") {
            this.path = [
                "/sources", 
                this.props.media.sid,
                "s" + this.props.media.season.toString().padStart(2, "0"),
                "e" + this.props.media.episode.toString().padStart(2, "0"),
            ].join("/")
        } else if (this.props.meta.type == "movie") {
            this.path = "/sources/" + this.props.media.sid
        }

        if (this.path != null) {
            this.setState({
                loaded: this.props.media.sid
            })
            this.state.hls.loadSource(this.path + "/out.m3u8")
        }
    }
    setUpIO() {
        window.socket.on("play", () => {
            console.log("remote play")
            document.getElementById("video").play()
        })
        window.socket.on("pause", () => document.getElementById("video").pause())
        window.socket.on("seek", time => this.props.store_update_time(time))
    }
}

const mapStateToProps = ({ player_reducer, session_reducer }) => {
    return {
        meta: player_reducer.meta,
        media: player_reducer.media,
        time: player_reducer.time,
        playing: player_reducer.playing,
        session_id: session_reducer.session_id
    }
}

const mapDispatchToProps = dispatch => {
    return {
        store_load_ep: (series, season, episode) => dispatch(ajaxEpisode({ series, season, episode })),
        store_load_movie: (sid) => dispatch(ajaxEpisode({ sid })),
        store_update_time: (time) => dispatch(updateTime(time)),
        store_update_playing: (playing) => dispatch(updatePlaying(playing))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Player))