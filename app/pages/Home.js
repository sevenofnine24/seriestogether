import React, { Component } from "react"
import axios from "../axios"
import MetaItem from "../components/MetaItem"

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            meta: []
        }

        this.fetch_meta()
    }
    render() {
        return <div id="meta" className="wrapper">
            <h2>Series</h2>
            <div id="meta_series">
                {this.listMetas("series")}
            </div>
            <h2>Movies</h2>
            <div id="meta_movies">
                {this.listMetas("movie")}
            </div>
        </div>
    }

    listMetas(type) {
        return this.state.meta.filter(x => x.type == type).map(x => 
            <MetaItem
                {...x}
                key={x.sid}
            />
        )
    }

    fetch_meta() {
        axios.get("/meta").then(response => {
            this.setState({
                meta: response.data
            })
        })
    }
}

export default Home