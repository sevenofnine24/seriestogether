import React, { Component } from "react"
import { useParams, withRouter, Link } from "react-router-dom"
import axios from "../axios"

class Series extends Component {
    constructor(props) {
        super(props)
        this.state = {
            series_id: this.props.match.params.id,
            meta: {},
            media: []
        }
        
        this.fetch_series_data()
    }

    render() {
        return <div id="single_series" className="wrapper">
            <div className="series_title title">{this.state.meta.title}</div>
            <div className="series_data">
                <div className="series_seasons">{this.count_seasons()} season(s)</div>
                <div className="series_episodes">{this.state.media.length} episode(s)</div>
            </div>
            {this.map_seasons()}
        </div>
    }

    count_seasons() {
        let set = new Set()
        this.state.media.forEach(x => {
            set.add(x.season)
        })

        return set.size
    }

    map_seasons() {
        let seasons = {}
        this.state.media.forEach(x => {
            if (!seasons.hasOwnProperty(x.season)) seasons[x.season] = []
            seasons[x.season].push(x)
        })

        return Object.keys(seasons).map(x => <div className="series_season" key={x}>
            <div className="season_title">Season {x}</div>
            <div className="season_episodes">
                {seasons[x].map(y => <Link className="season_episode" to={this.getLink(y)} key={y.episode}>
                    <div className="episode_number">Episode {y.episode}</div>
                    <div className="episode_title">{y.title}</div>
                </Link>)}
            </div>
        </div>)
    }

    getLink(media) {
        return "/player/" + [media.sid, media.season, media.episode].join("/")
    }

    fetch_series_data() {
        axios.get("/media/" + this.state.series_id).then(response => {
            this.setState({
                meta: response.data.meta,
                media: response.data.media
            })
        })
    }
}

export default withRouter(Series)