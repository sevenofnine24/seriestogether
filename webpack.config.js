const path = require("path");

module.exports = {
    mode: "development",
    entry: "./app/App.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.js",
        publicPath: "/scripts/",
        libraryTarget: "umd"
    },
    module: {
      rules: [
        {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            }
        },
        {
            test: /\.s[ac]ss$/i,
            use: [
                "style-loader",
                "css-loader",
                "sass-loader",
            ],
          },
      ]
    },
    devServer: {
        contentBase: path.join(__dirname, "public"),
        compress: true,
        historyApiFallback: true,
        hot: true,
        disableHostCheck: true
    }
  };