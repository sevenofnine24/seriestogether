const mongoose = require("mongoose")
const meta_model = require("./models/meta_model")
const session_model = require("./models/session_model")
const media_model = require("./models/media_model")

const connect = () => {
    return mongoose.connect("mongodb://localhost:27017/seriestogether")
}

module.exports = {
    connect,
    models: {
        meta_model,
        session_model,
        media_model
    }
}