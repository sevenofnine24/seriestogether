module.exports = (media_model, meta_model) => {
    const express = require("express")
    const controller = express()

    // helpers
    const with_meta = async (sid, find = {}, one = false) => {
        const media_data = await media_model.find(Object.assign(find, { sid }))
        const meta_data = await meta_model.find({ sid })
        const media_return = one ? media_data[0] : media_data
        return {
            meta: meta_data[0],
            media: media_return
        }
    }
    
    controller.get("/:sid/nometa", async (req, res) => {
        res.json(await media_model.find({ sid: req.params.sid }))
    })

    controller.get("/:sid", async (req, res) => {
        res.json(await with_meta(req.params.sid))
    })

    controller.get("/:sid/:season/:episode", async(req, res) => {
        res.json(await with_meta(req.params.sid, {
            season: req.params.season,
            episode: req.params.episode
        }, true))
    })

    return controller
}