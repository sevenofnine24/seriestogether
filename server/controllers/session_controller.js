const Session = require('../models/session_model');

const io = require('socket.io')(3001);

module.exports = (model) => {
    /*const express = require("express")
    const controller = express()*/
    io.origins('*:*')
    io.on("connect", socket => {
        socket.on("join", (room)=> {
            socket.join(room)
            console.log("client joined " + room)
        })

        socket.on("play", (room) => {
            console.log("client played " + room)
            socket.to(room).emit("play")
        })

        socket.on("pause", (room) => {
            console.log("client paused " + room)
            socket.to(room).emit("pause")
        })

        socket.on("seek", (room, time) => {
            socket.to(room).emit("seek", time)
        })
    })
}