module.exports = (model) => {
    const express = require("express")
    const controller = express()

    const return_fields = ["sid", "title", "image", "type"]

    controller.get("/", async (req, res) => {
        res.json(await model.find({}, return_fields))
    })

    controller.get("/series", async (req, res) => {
        res.json(await model.find({ type: "series" }, return_fields))
    })

    controller.get("/movies", async (req, res) => {
        res.json(await model.find({ type: "movies" }, return_fields))
    })

    controller.get("/:sid", async (req, res) => {
        const result = await model.find({ sid: req.params.sid })
        if (result.length > 0) res.json(result[0])
        else res.status(400)
    })

    return controller
}