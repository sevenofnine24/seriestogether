const express = require("express")
const mongo = require("./models")
const media_controller = require("./controllers/media_controller")
const meta_controller = require("./controllers/meta_controller")
const session_controller = require("./controllers/session_controller")
const cors = require("cors")

const app = express()
const port = 3000

app.use(cors())
app.use("/", express.static("./public"))
app.use("/scripts", express.static("./dist/"))
app.use("/meta", meta_controller(mongo.models.meta_model))
app.use("/media", media_controller(mongo.models.media_model, mongo.models.meta_model))

mongo.connect().then(async () => {
    session_controller(mongo.models.session_model)
    app.listen(port, '0.0.0.0', () => {
        console.log(`Example app listening at http://localhost:${port}`)
    })
})
