const mongoose = require("mongoose")

const metaSchema = new mongoose.Schema(
    {
        sid: String,
        title: String,
        image: String,
        type: String
    }
)

const Meta = mongoose.model("Meta", metaSchema, "meta")
module.exports = Meta