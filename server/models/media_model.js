const mongoose = require("mongoose")

const mediaSchema = new mongoose.Schema(
    {
        sid: String,
        type: String,
        season: Number,
        episode: Number,
        title: String,
        path: String
    }
)

const Media = mongoose.model("Media", mediaSchema, "media")
module.exports = Media