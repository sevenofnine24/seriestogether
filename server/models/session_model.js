const mongoose = require("mongoose")

const clientSchema = new mongoose.Schema(
    {
        ip: String,
        id: String
    }
)

const mediaSchema = new mongoose.Schema(
    {
        sid: String,
        type: String,
        season: Number,
        episode: Number,
        title: String,
        path: String
    }
)

const sessionSchema = new mongoose.Schema(
    {
        id: String,
        playing: mediaSchema,
        master: clientSchema,
        slaves: [clientSchema]
    }
)

const Session = mongoose.model("session", sessionSchema)
module.exports = Session